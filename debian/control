Source: powertop
Section: utils
Priority: extra
Maintainer: Jose Luis Rivas <ghostbar@debian.org>
Uploaders: Patrick Winnertz <winnie@debian.org>, Julian Wollrath <jwollrath@web.de>
Build-Depends: automake, debhelper (>= 9), dh-autoreconf, libncurses5-dev, libncursesw5-dev, libnl-3-dev, libnl-genl-3-dev, libpci-dev, pkg-config
Standards-Version: 3.9.5
Vcs-git: git://github.com/ghostbar/powertop.deb.git
Vcs-browser: https://github.com/ghostbar/powertop.deb
Homepage: https://01.org/powertop/

Package: powertop
Architecture: alpha amd64 armel armhf hppa i386 m68k mips mipsel powerpc powerpcspe ppc64 s390 s390x sh4 sparc sparc64 x32
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: cpufrequtils, laptop-mode-tools
Description: diagnose issues with power consumption and management
 PowerTOP is a Linux tool to diagnose issues with power consumption and
 power management. In addition to being a diagnostic tool, PowerTOP also
 has an interactive mode you can use to experiment with various power
 management settings, for cases where the Linux distribution has not
 enabled those settings.
 .
 PowerTOP reports which components in the system are most likely to blame
 for higher-than-needed power consumption, ranging from software
 applications to active components in the system. Detailed screens are
 available for CPU C and P states, device activity, and software activity.

Package: powertop-dbg
Architecture: alpha amd64 armel armhf hppa i386 m68k mips mipsel powerpc powerpcspe ppc64 s390 s390x sh4 sparc sparc64 x32
Section: debug
Depends: powertop (=${binary:Version}), ${misc:Depends}
Description: debugging symbols for powertop
 PowerTOP is a Linux tool to diagnose issues with power consumption and
 power management. In addition to being a diagnostic tool, PowerTOP also
 has an interactive mode you can use to experiment with various power
 management settings, for cases where the Linux distribution has not
 enabled those settings.
 .
 PowerTOP reports which components in the system are most likely to blame
 for higher-than-needed power consumption, ranging from software
 applications to active components in the system. Detailed screens are
 available for CPU C and P states, device activity, and software activity.
 .
 This package contains the debugging symbols for powertop.
